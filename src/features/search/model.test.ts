import { allSettled, fork } from "effector";

import {
  $search,
  searchButtonClicked,
  searchChanged,
  searchDomain,
  startSearchFx,
} from "./model";

describe("search", () => {
  test("search button click should start search", async () => {
    const startSearchMock = jest.fn();

    const scope = fork(searchDomain, {
      handlers: new Map().set(startSearchFx, startSearchMock),
    });

    await allSettled(searchButtonClicked, { scope, params: undefined });

    expect(startSearchMock).toHaveBeenCalledTimes(1);
  });

  test("search store should be clean after search started 2", async () => {
    const startSearchMock = jest.fn();

    const TEST_SEARCH_STRING = "Хочу в Пхукет!";

    const scope = fork(searchDomain, {
      handlers: new Map().set(startSearchFx, startSearchMock),
    });

    await allSettled(startSearchFx, { scope, params: TEST_SEARCH_STRING });

    expect(startSearchMock).toHaveBeenCalledTimes(1);
    expect(startSearchMock).toHaveBeenCalledWith(TEST_SEARCH_STRING);

    const searchState = scope.getState($search.map((search) => search));

    expect(searchState).toEqual("");
  });

  test("search change event should change store value", async () => {
    const TEST_SEARCH_STRING = "Хочу в Пхукет!";

    const scope = fork(searchDomain);

    await allSettled(searchChanged, { scope, params: TEST_SEARCH_STRING });

    const searchState = scope.getState($search.map((search) => search));

    expect(searchState).toEqual(TEST_SEARCH_STRING);
  });
});
