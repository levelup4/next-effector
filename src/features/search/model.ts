import { createDomain, forward, sample } from "effector";
import { createGate } from "effector-react";

const SearchFormGate = createGate();
const searchDomain = createDomain();

const searchChanged = searchDomain.createEvent<string>();
const searchReset = searchDomain.createEvent();
const searchButtonClicked = searchDomain.createEvent();

const $search = searchDomain
  .createStore("")
  .on(searchChanged, (_oldSearch, newSearch) => newSearch)
  .reset(searchReset);

const startSearchFx = searchDomain.createEffect<string, void, Error>({
  handler: (search: string) => {
    console.log("Поиск начался!", search);
  },
});

sample({
  source: $search,
  target: startSearchFx,
  clock: searchButtonClicked,
});
forward({ from: startSearchFx, to: searchReset });

export {
  $search,
  searchChanged,
  searchButtonClicked,
  searchDomain,
  startSearchFx,
  SearchFormGate,
};
