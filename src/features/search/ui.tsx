import { useGate, useStore } from "effector-react";
import React from "react";
import {
  $search,
  searchButtonClicked,
  searchChanged,
  SearchFormGate,
} from "./model";

export const SearchForm: React.FC = () => {
  useGate(SearchFormGate);
  const search = useStore($search);

  const handleSearch = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    searchButtonClicked();
  };

  return (
    <div>
      <h1>Search something</h1>
      <form onSubmit={handleSearch}>
        <input
          type="search"
          value={search}
          onChange={(e) => searchChanged(e.target.value)}
        />
        <button type="submit">Search</button>
      </form>
    </div>
  );
};
