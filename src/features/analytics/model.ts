import { createDomain, guard, sample } from "effector";
import * as Analytics from "./lib";

const analyticsDomain = createDomain();

const initAnalyticsFx = analyticsDomain.createEffect({
  handler: Analytics.initAnalytics,
});

const sendEventFx = analyticsDomain.createEffect({
  handler: Analytics.sendEvent,
});
const sendManyEventsFx = analyticsDomain.createEffect({
  handler: (events: Analytics.AnalyticsEvent[]) =>
    Promise.all(events.map(sendEventFx)),
});

const sendDataToAnalytics =
  analyticsDomain.createEvent<Analytics.AnalyticsEvent>();
const sendEventToQueue =
  analyticsDomain.createEvent<Analytics.AnalyticsEvent>();
const delayedEventsSent = analyticsDomain.createEvent();

const $inited = analyticsDomain
  .createStore(false)
  .on(initAnalyticsFx.done, () => true);

const $notInited = $inited.map((inited) => !inited);

const $delayedEvents = analyticsDomain
  .createStore<Analytics.AnalyticsEvent[]>([])
  .on(sendEventToQueue, (events, newEvent) => [...events, newEvent])
  .reset(delayedEventsSent);

/**
 * Inited case
 *
 *   If analytics has been already inited, send event
 */
guard({ source: sendDataToAnalytics, target: sendEventFx, filter: $inited });

/**
 * Not-inited case
 *
 *   If analytics is not inited, put event to queue
 */
guard({
  source: sendDataToAnalytics,
  target: sendEventToQueue,
  filter: $notInited,
});

sample({
  clock: initAnalyticsFx.done,
  source: $delayedEvents,
  target: [sendManyEventsFx, delayedEventsSent],
});

export { analyticsDomain, sendDataToAnalytics, initAnalyticsFx, sendEventFx };
