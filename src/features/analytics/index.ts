import { sendDataToAnalytics, initAnalyticsFx } from "./model";

const analyticsService = {
  sendEvent: sendDataToAnalytics,
  initFx: initAnalyticsFx,
};

export { analyticsService };
